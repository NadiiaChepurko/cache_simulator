#ifndef __c_sim_v_2__cache__
#define __c_sim_v_2__cache__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct cache{
    char d;
    char v;
    unsigned int tag;
}cache_t;

typedef struct dataP{
    unsigned int hits;
    unsigned int misses;
    unsigned int reads;
    unsigned int writes;
}data_t;

typedef struct node{
    unsigned int address;
    struct node *next;
    struct node *prev;
}node_t;

typedef struct set{
    int current_size;
    int last_free_index;
    node_t *front;
    node_t *rear;
}set_t;

unsigned int htoi (const char *ptr);
void direct_associative(data_t *data, int number_of_blocks, int block_size, char policy, const char *filename_ptr);
void fully_associative(data_t *data, int number_of_blocks, int block_size, char policy, const char *filename_ptr);
void nway_associative(data_t *data, int number_of_blocks, int block_size, char policy, int n, const char *filename_ptr);
unsigned int get_address(char *str);

/*Helper methods*/

/*fully associative*/
void update_usage(unsigned int tag);
unsigned int get_least_used_tag();

/*nway*/
void update_usage_for_set(unsigned int tag, node_t *front, node_t *rear, int cache_set_number);
unsigned int get_least_used_tag_for_set(node_t *front, int cache_set_number);

/*general*/
void printLL(node_t *front_local);

int logarithm(unsigned int x);

#endif