#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "cache.h"

int isPowerOfTwo (unsigned int x);
int get_n(char *str);

/*
 c-sim [-h] <cache size> <associativity> <block size> <write policy> <trace file>
 */

int main(int argc, const char * argv[]) {
    int cache_size; /*size of our cache in bytes*/
    char associativity; /*direct, fully associative, nway associative*/
    int block_size; /*size of a cache line in bytes*/
    char write_policy; /*write-though or write-back*/
    data_t *data; /*our final result after file trace*/
    int number_of_blocks; /*to be calculated based on cache size of block size*/
    char *assoc; /*argument of nway associative format, we need it to get n*/
    
    /*check number of arguments*/
    if(argc == 2){
        if(strcmp(argv[1], "-h") == 0){
            printf("Invocation interface: c-sim [-h] <cache size> <associativity> <block size> <write policy> <trace file>\n");
            return 1;
        }else{
            printf("Wrong invokation. Type  c-sim -h for invokation interface.\n");
            return 1;
        }
    }else if(argc != 6){
        printf("Wrong invokation. Type  c-sim -h for invokation interface.\n");
        return 1;
    }
    
    /*get cache size*/
    cache_size = atoi(argv[1]);
    if(!isPowerOfTwo(cache_size)){
        printf("ERROR: cache size must be a power of two. Type  c-sim -h for invokation interface.\n");
        return 1;
    }
    
    /*get cache associativity*/
    if(!(strcmp(argv[2], "direct") == 0 || strcmp(argv[2], "assoc") == 0 || strstr(argv[2], "assoc:") != NULL)){
        printf("ERROR: wrong associativity format.  Type  c-sim -h for invokation interface. \n");
        return 1;
    }
    associativity = strcmp(argv[2], "direct") == 0 ? 'd' : (strcmp(argv[2], "assoc") == 0 ? 'a' : 'n');
    
    /*get block size*/
    block_size = atoi(argv[3]);
    if(!isPowerOfTwo(block_size)){
        printf("ERROR: block size must be a power of two. Type  c-sim -h for invokation interface. \n");
        return 1;
    }
    
    /*define write policy*/
    if(!(strcmp(argv[4], "wt") == 0 || strcmp(argv[4], "wb") == 0)){
        printf("ERROR: wrong write policy format.  Type  c-sim -h for invokation interface.\n");
        return 1;
    }
    write_policy = strcmp(argv[4], "wt") == 0 ? 't' : 'b';
    
    /*initialize our data struct*/
    data = (data_t*)calloc(1, sizeof(data_t));

    /*calculate number of blocks, works for direct and associative, will need to divide result on n for n-way*/
    number_of_blocks = cache_size / block_size;
    
    /*call function for a given associativity*/
    if (associativity == 'd'){ /*direct associative*/
        direct_associative(data, number_of_blocks, block_size, write_policy, argv[5]);
    }else if(associativity == 'a'){ /*fully associative*/
        fully_associative(data, number_of_blocks, block_size, write_policy, argv[5]);
    }else{
        assoc = (char *)calloc(1, sizeof(argv[2])+1);
        assoc = strcpy(assoc, argv[2]);
        nway_associative(data, number_of_blocks, block_size, write_policy, get_n(assoc), argv[5]);
        free(assoc);
    }
    
    
    printf("Memory reads: %d \n", data->reads);
    printf("Memory writes: %d \n", data->writes);
    printf("Cache hits: %d \n", data->hits);
    printf("Cache misses: %d \n", data->misses);
    
    return 0;
}


int isPowerOfTwo (unsigned int x)
{
    return (
            x == 1 || x == 2 || x == 4 || x == 8 || x == 16 || x == 32 ||
            x == 64 || x == 128 || x == 256 || x == 512 || x == 1024 ||
            x == 2048 || x == 4096 || x == 8192 || x == 16384 ||
            x == 32768 || x == 65536 || x == 131072 || x == 262144 ||
            x == 524288 || x == 1048576 || x == 2097152 ||
            x == 4194304 || x == 8388608 || x == 16777216 ||
            x == 33554432 || x == 67108864 || x == 134217728 ||
            x == 268435456 || x == 536870912 || x == 1073741824 );
}

int get_n(char *str){
    char *strPtr;
    int clean;
    
    strPtr = str+6;
    clean = 0;
    while(*strPtr != '\0'){
        if(!isdigit(*strPtr)){
            *strPtr = '\0';
            clean = 1;
        }
        if(clean){
            *strPtr = '\0';
        }
        strPtr++;
    }
    return atoi(str+6);
}
