#include "cache.h"

/*direct mapping assigned each memory block to a specific line in the cache.
 If a line is all ready taken up by a memory block when a new block needs to be loaded, the old block is trashed. */

void direct_associative(data_t *data, int number_of_blocks, int block_size, char policy, const char *filename_ptr){
    /*DECLARATION*/
    cache_t *cache;     /*our cache struct*/
    char *buffer;       /*will hold our trace file line*/
    char instruction;   /*R or W*/
    unsigned int full_address;     /*extracted from trace file*/
    unsigned int index_and_tag;    /*address without offset bits*/
    int index;                          /*index bits for our cache*/
    unsigned int tag;              /*bits that are left after offset and index bits*/
    FILE *fp; /*trace file pointer*/
    
    /*INITIALIZATION*/
    fp = fopen(filename_ptr, "r");
    if(fp == NULL){
        printf("ERROR: cannot open the file \n");
        exit(-1);
    }
    
    cache = (cache_t *)calloc(number_of_blocks + 1, sizeof(cache_t));
    full_address = 0;
    index = 0;
    buffer = (char *)calloc(1024, 1);
    
    /*MAIN ALGORITHM*/
    
    while ( fgets(buffer, 1023, fp) != NULL && strstr(buffer, "#") == NULL ){ /*get our cache line*/
        instruction = strstr(buffer, "W") == NULL ? 'r' : 'w';
        full_address = get_address(buffer);
        index_and_tag = full_address >> logarithm(block_size);
        index = index_and_tag & (number_of_blocks - 1);
        tag = index_and_tag >> logarithm(number_of_blocks);
        
        if(policy == 't'){                      /*write-through policy*/
            if(instruction == 'w'){             /*write instruction*/
                data->writes++;                 /*in write-though we write to a memory in any case*/
                if (cache[index].tag == tag){
                    data->hits++;
                }else{                          /*cache write miss*/
                    
                    data->reads++;              /*<<<<<<<<<<<<<<<<----ADDED FOR MEMORY READS----*/
                    
                    data->misses++;
                    cache[index].tag = tag;
                    cache[index].v = 1;
                }
            }else{                              /*read instruction*/
                if(cache[index].tag == tag){
                    data->hits++;               /*data is valid, read from cache, does not go to RAM*/
                }else{                          /*no data yet, read from DRAM*/
                    data->misses++;
                    data->reads++;
                    cache[index].tag = tag;     /*bring the entry to a cache because of temporal locality*/
                    cache[index].v = 1;
                }
            }
        }else{/*write-back policy*/
            if(instruction == 'w'){
                if (cache[index].v == 1 && cache[index].tag == tag){   /*we write for the same address space*/
                    data->hits++;               /*here we have cache hit and we modify our data thus need to set dirty bit*/
                    cache[index].d = 1;
                    cache[index].v = 1;
                }else{
                    
                    data->misses++;
                    data->reads++;              /*<<<<<<<<<<<<<<<<----ADDED FOR MEMORY READS----*/
                    
                    /*we need to rewrite data of previous tag*/
                    if(cache[index].d == 1 && cache[index].v == 1) data->writes++; /*our dirty bit is set meaning that we should update DRAM*/
                    cache[index].tag = tag;     /*set new tag*/
                    cache[index].v = 1;
                    cache[index].d = 1;         /*<<< if here we have dirty bit as 1 - output is right. first time set, data is consistent*/
                }
            }else{                              /*read instruction for wb policy*/
                if(cache[index].tag == tag){
                    data->hits++;               /*data is valid, read from cache, does not go to DRAM*/
                }else{                          /*read DRAM*/
                    data->misses++;
                    data->reads++;
                    if(cache[index].d == 1 && cache[index].v == 1) data->writes++; /*<<<<<<<<<<<<------- ADDED */
                    cache[index].tag = tag;     /*bring the entry to a cache because of temporal locality*/
                    cache[index].v = 1;
                    cache[index].d = 0;         /*first time set, data is consistent*/
                }
            }
        }
    }
}


/*------------------------------------------Fully associative cache--------------------------------------------*/

/*global data*/
node_t *global_front;
node_t *global_rear;

void fully_associative(data_t *data, int number_of_blocks, int block_size, char policy, const char *filename_ptr){
    cache_t *cache;
    int current_cache_size, last_free_index, hit, i;
    char *buffer;
    char instruction;
    unsigned int full_address;
    unsigned int least_used_index_and_tag;
    unsigned int index_and_tag;
    FILE *fp;
    
    cache = (cache_t *)calloc(number_of_blocks + 1, sizeof(cache_t));
    
    global_front = NULL;
    global_rear = NULL;
    current_cache_size = 0;
    last_free_index = 0;
    hit = 0;
    full_address = 0;
    buffer = (char *)calloc(1024, 1);
    
    fp = fopen(filename_ptr, "r");
    if(fp == NULL){
        printf("ERROR: cannot open the file \n");
        exit(-1);
    }
    i = 0;

    while ( fgets(buffer, 1023, fp) != NULL && strstr(buffer, "#") == NULL ){
        instruction = strstr(buffer, "W") == NULL ? 'r' : 'w';
        full_address = get_address(buffer);
        index_and_tag = full_address >> (int)logarithm(block_size);
        
        if(policy == 't'){                  /*wt policy for fully assoc*/
            if(instruction == 'w'){
                data->writes++;             /*in wt we write in any case to our memory*/
                
                /*go and check if we already have this address in our cache*/
                hit = 0;
                for(i = 0; i < current_cache_size; i++){
                    if(cache[i].v == 1 && cache[i].tag == index_and_tag){
                        data->hits++;
                        hit = 1;
                        update_usage(index_and_tag);
                        break;
                    }
                }
                /*we did not find this address in our cache*/
                if(hit == 0){
                    data->misses++;
                    
                    data->reads++;              /*<<<<<<<<<<<<<<<<----ADDED FOR MEMORY READS----*/
                    
                    /*our cache is full, need to evict least recently used address*/
                    if(number_of_blocks <= current_cache_size){
                        
                        least_used_index_and_tag = get_least_used_tag();
                        
                        for(i = 0; i < number_of_blocks; i++){
                            if(cache[i].v == 1 && cache[i].tag == least_used_index_and_tag){
                                cache[i].tag = index_and_tag;
                                cache[i].v = 1;
                                update_usage(index_and_tag);
                                break;
                            }
                        }
                    }else{ /*our cache is not full yet, fill first free spot*/
                        cache[last_free_index].tag = index_and_tag;
                        cache[last_free_index].v = 1;
                        update_usage(index_and_tag);
                        current_cache_size++;
                        last_free_index++;
                    }
                }
            }else{ /*read instruction*/
                /*check if we already have this address in our cache*/
                hit = 0;
                for(i = 0; i < current_cache_size; i++){
                    if(cache[i].v == 1 && cache[i].tag == index_and_tag){
                        data->hits++;
                        hit = 1;
                        update_usage(index_and_tag);
                        break;
                    }
                }
                if(hit == 0){
                    data->reads++;
                    data->misses++;
                    
                    /*need to bring the data to our cache due to a temporal locality*/
                    if(number_of_blocks <= current_cache_size){
                        
                        least_used_index_and_tag = get_least_used_tag();
                        
                        for(i = 0; i < number_of_blocks; i++){
                            if(cache[i].v == 1 && cache[i].tag == least_used_index_and_tag){
                                cache[i].tag = index_and_tag;
                                cache[i].v = 1;
                                update_usage(index_and_tag);
                                break;
                            }
                        }
                    }else{ /*our cache is not full yet, fill first free spot*/
                        cache[last_free_index].tag = index_and_tag;
                        cache[last_free_index].v = 1;
                        update_usage(index_and_tag);
                        current_cache_size++;
                        last_free_index++;
                    }
                }
            }
            
        }else{ /*wb policy for fully assoc*/
            if(instruction == 'w'){
                /*go and check if we already have this address in our cache*/
                hit = 0;
                for(i = 0; i < current_cache_size; i++){
                    if(cache[i].v == 1 && cache[i].tag == index_and_tag){
                        data->hits++;
                        hit = 1;
                        update_usage(index_and_tag);
                        cache[i].d = 1;                 /*we modified our data*/
                        cache[i].v = 1;
                        break;
                    }
                }
                
                /*we did not find this address in our cache*/
                if(hit == 0){
                    data->misses++;
                    
                    data->reads++;              /*<<<<<<<<<<<<<<<<----ADDED FOR MEMORY READS----*/
                    
                    /*our cache is full, need to evict least recently used address*/
                    if(number_of_blocks <= current_cache_size){
                        
                        least_used_index_and_tag = get_least_used_tag();
                        
                        for(i = 0; i < number_of_blocks; i++){
                            if(cache[i].v == 1 && cache[i].tag == least_used_index_and_tag){
                                if(cache[i].d == 1) data->writes++;   /*we should write to DRAM only if data is inconsistent*/
                                cache[i].tag = index_and_tag;
                                cache[i].d = 1;                       /*!!!!!we rewrite data for a new tag and it is consistent for now*/
                                cache[i].v = 1;
                                update_usage(index_and_tag);
                                break;
                            }
                        }
                    }else{/*our cache is not full yet, full first free spot*/
                        cache[last_free_index].tag = index_and_tag;
                        cache[last_free_index].v = 1;
                        cache[last_free_index].d = 1;                 /*!!! added here. write for the first time, data is consistent*/
                        update_usage(index_and_tag);
                        current_cache_size++;
                        last_free_index++;
                    }
                }
            }else{ /*read instruction for wb policy*/
                /*check if we already have this address in our cache*/
                hit = 0;
                for(i = 0; i < current_cache_size; i++){
                    if(cache[i].v == 1 && cache[i].tag == index_and_tag){
                        data->hits++;
                        hit = 1;
                        update_usage(index_and_tag);
                        break;
                    }
                }
                if(hit == 0){
                    data->reads++; /*memory reads*/
                    data->misses++; /*cache misses*/
                    
                    if(number_of_blocks <= current_cache_size){
                        
                        least_used_index_and_tag = get_least_used_tag();
                        
                        for(i = 0; i < number_of_blocks; i++){
                            if(cache[i].v == 1 && cache[i].tag == least_used_index_and_tag){
                                if(cache[i].d == 1) data->writes++;   /*we should write to DRAM only if data is inconsistent*/
                                cache[i].tag = index_and_tag;
                                cache[i].d = 0;                       /*we rewrite data for a new tag and it is consistent for now*/
                                cache[i].v = 1;
                                update_usage(index_and_tag);
                                break;
                            }
                        }
                    }else{/*our cache is not full yet, fill first free spot*/
                        cache[last_free_index].tag = index_and_tag;
                        cache[last_free_index].v = 1;
                        cache[last_free_index].d = 0;                 /*write for the first time, data is consistent*/
                        update_usage(index_and_tag);
                        current_cache_size++;
                        last_free_index++;
                    }
                    
                }
                
            } /*read instruction for wb policy else close*/
        }
    }
}


/*-----------------------------------------Nway associative cache-----------------------------------------------*/
set_t **set_data_global;
void nway_associative(data_t *data, int number_of_blocks, int block_size, char policy, int n, const char *filename_ptr){
    /*DECLARATION*/
    int number_of_sets;
    int i;              /*used for iterations*/
    int hit;            /*boolean value to identify hit event*/
    cache_t **cache;    /*2D cache structure*/
    set_t **set_data;   /*here we will store data for each set, such as linkedlist pointers and sizes*/
    char *buffer;
    char instruction;
    unsigned int full_address;
    unsigned int index_and_tag;
    int index;
    unsigned int tag;
    unsigned int least_used_tag;   /*least used tag within a particular set*/
    FILE *fp;
    
    /*INITIALIZATION*/
    
    fp = fopen(filename_ptr, "r");
    if(fp == NULL){
        printf("ERROR: cannot open the file \n");
        exit(-1);
    }
    
    number_of_sets = number_of_blocks/n;
    least_used_tag = 0;
    buffer = (char *)calloc(1024, 1);
    full_address = 0;
    index = -1;
    hit = 0;
    i = 0;
    
    set_data = (set_t **)calloc(number_of_sets + 1, sizeof(set_t *));
    
    cache = (cache_t **)calloc(number_of_sets + 1, sizeof(cache_t *));
    
    for(i = 0; i < number_of_sets; i++){
        set_data[i] = (set_t *)calloc(n+1, sizeof(set_t));
    }
    for(i = 0; i < number_of_sets; i++){
        cache[i] = (cache_t *)calloc(n+1, sizeof(cache_t));
    }
    
    set_data_global = set_data;     /*we want this struct to be visible for helper methods*/
    
    while ( fgets(buffer, 1023, fp) != NULL && strstr(buffer, "#") == NULL ){
        instruction = strstr(buffer, "W") == NULL ? 'r' : 'w';
        full_address = get_address(buffer);
        index_and_tag = full_address >> (int)logarithm(block_size);
        index = index_and_tag & (number_of_sets - 1);
        tag = index_and_tag >> (int)logarithm(number_of_sets);
        
        if(policy == 't'){      /*write-though nway cache*/
            if(instruction == 'w'){
                data->writes++; /*for wt we write in any case*/
                /*go and check if we already have this address in our cache*/
                hit = 0;
                for(i = 0; i < set_data[index]->current_size; i++){
                    if(cache[index][i].v == 1 && cache[index][i].tag == tag){
                        data->hits++;                   /*we found our tag in the cache*/
                        hit = 1;
                        update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                        break;
                    }
                }
                
                if(hit == 0){ /*we did not find the tag in our cache set*/
                    data->misses++;
                    
                    data->reads++;              /*<<<<<<<<<<<<<<<<----ADDED FOR MEMORY READS----*/
                    
                    
                    /*if our set is full, we need to evict least recently used address*/
                    if(n <= set_data[index]->current_size){
                        least_used_tag = get_least_used_tag_for_set(set_data[index]->front, index);
                        for(i = 0; i < n; i++){
                            if(cache[index][i].tag == least_used_tag){  /*evict one and replace with another*/
                                cache[index][i].tag = tag;
                                cache[index][i].v = 1;
                                update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                                break;
                            }
                        }

                    }else{ /*our set is not full yet, we can find first free spot*/
                        cache[index][set_data[index]->last_free_index].tag = tag;
                        cache[index][set_data[index]->last_free_index].v = 1;
                        update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                        (set_data[index]->current_size)++;
                        (set_data[index]->last_free_index)++;
                    }
                }
            }else{ /*read instrcution*/
                /*check if we already have this address in our set*/
                hit = 0;
                for(i = 0; i < set_data[index]->current_size; i++){
                    if(cache[index][i].v == 1 && cache[index][i].tag == tag){
                        data->hits++;
                        hit = 1;
                        update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                        break;
                    }
                }
                if(hit == 0){
                    data->reads++;
                    data->misses++;
                    
                    /*bring data from DRAM to the cache because of temporal locality*/
                    /*if our set is full, we need to evict least recently used address*/
                    if(n <= set_data[index]->current_size){
                        least_used_tag = get_least_used_tag_for_set(set_data[index]->front, index);
                        for(i = 0; i < n; i++){
                            if(cache[index][i].tag == least_used_tag){  /*evict one and replace with another*/
                                cache[index][i].tag = tag;
                                cache[index][i].v = 1;
                                update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                                break;
                            }
                        }
                        
                    }else{ /*our set is not full yet, we can find first free spot*/
                        cache[index][set_data[index]->last_free_index].tag = tag;
                        cache[index][set_data[index]->last_free_index].v = 1;
                        update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                        (set_data[index]->current_size)++;
                        (set_data[index]->last_free_index)++;
                    }
                }
            }
            
        }else{   /*write-back nway cache*/
            if(instruction == 'w'){
                /*go and check if we already have this address in our cache*/
                hit = 0;
                for(i = 0; i < set_data[index]->current_size; i++){
                    if(cache[index][i].v == 1 && cache[index][i].tag == tag){
                        data->hits++;
                        hit = 1;
                        update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                        cache[index][i].d = 1;      /*we update data in our cache and thus data is inconsistent*/
                        break;
                    }
                }
                
                /*we did not find this address in our set*/
                if(hit == 0){
                    data->misses++;
                    
                    data->reads++;              /*<<<<<<<<<<<<<<<<----ADDED FOR MEMORY READS----*/
                    
                    
                    /*our set is full, need to evict least recently used address*/
                    if(n <= set_data[index]->current_size){
                        least_used_tag = get_least_used_tag_for_set(set_data[index]->front, index);
                        for(i = 0; i < n; i++){
                            if(cache[index][i].v == 1 && cache[index][i].tag == least_used_tag){
                                if(cache[index][i].d == 1) data->writes++;
                                cache[index][i].tag = tag;
                                cache[index][i].d = 1;          /*we updated cache with consistent new data*/
                                cache[index][i].v = 1;
                                update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                                break;
                            }
                        }
                    }else{
                        /*our set is not full yet, full first free spot*/
                        cache[index][set_data[index]->last_free_index].tag = tag;
                        cache[index][set_data[index]->last_free_index].v = 1;
                        cache[index][set_data[index]->last_free_index].d = 1;    /*we updated cache with consistent new data*/
                        update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                        (set_data[index]->current_size)++;
                        (set_data[index]->last_free_index)++;
                    }
                    
                }
            }else{  /*read instruction*/
                /*check if we already have this address in our set*/
                hit = 0;
                for(i = 0; i < set_data[index]->current_size; i++){
                    if(cache[index][i].v == 1 && cache[index][i].tag == tag){
                        data->hits++;
                        hit = 1;
                        update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                        break;
                    }
                }
                if(hit == 0){
                    data->reads++;
                    data->misses++;
                    
                    /*our set is full, need to evict least recently used address*/
                    if(n <= set_data[index]->current_size){
                        least_used_tag = get_least_used_tag_for_set(set_data[index]->front, index);
                        for(i = 0; i < n; i++){
                            if(cache[index][i].v == 1 && cache[index][i].tag == least_used_tag){
                                if(cache[index][i].d == 1) data->writes++;
                                cache[index][i].tag = tag;
                                cache[index][i].d = 0;          /* !!!! we updated cache with consistent new data*/
                                cache[index][i].v = 1;
                                update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                                break;
                            }
                        }
                    }else{
                        /*our set is not full yet, full first free spot*/
                        cache[index][set_data[index]->last_free_index].tag = tag;
                        cache[index][set_data[index]->last_free_index].v = 1;
                        cache[index][set_data[index]->last_free_index].d = 0;    /*we updated cache with consistent new data*/
                        update_usage_for_set(tag, set_data[index]->front, set_data[index]->rear, index);
                        (set_data[index]->current_size)++;
                        (set_data[index]->last_free_index)++;
                    }
                }
            }
        }
    }
}

/*CONVERTION*/
unsigned int htoi (const char *ptr)
{
    unsigned int value;
    char ch;
    
    value = 0;
    ch = *ptr;
    while (ch == ' ' || ch == '\t')
        ch = *(++ptr);
    
    for (;;) {
        
        if (ch >= '0' && ch <= '9')
            value = (value << 4) + (ch - '0');
        else if (ch >= 'A' && ch <= 'F')
            value = (value << 4) + (ch - 'A' + 10);
        else if (ch >= 'a' && ch <= 'f')
            value = (value << 4) + (ch - 'a' + 10);
        else
            return value;
        ch = *(++ptr);
    }
}

unsigned int get_address(char *str){
    while(*str != 'W' && *str != 'R'){
        str++;
    }
    str = str+4;
    return htoi(str);
}

/*----------------------------------HELPER METHODS FOR NWAY ASSOCIATIVE-----------------------------------*/

void update_usage_for_set(unsigned int tag, node_t *front, node_t *rear, int cache_set_number){
    
    node_t *temp;
    int found;
    
    if(front == NULL){
        front = (node_t *)calloc(1, sizeof(node_t));
        front->prev = NULL;
        front->next = NULL;
        rear = front;
        front->address = tag;
        set_data_global[cache_set_number]->front = front;
        set_data_global[cache_set_number]->rear = rear;
        return;
    }

    if(rear->address == tag) return; /*already has the most recently used priority*/
    
    found = 0;
    
    if(front->address == tag && front != rear){
        temp = front;
        front = front->next;
        front->prev = NULL;
        
        rear->next = temp;
        temp->prev = rear;
        rear = temp;
        rear->next = NULL;
        set_data_global[cache_set_number]->front = front;
        set_data_global[cache_set_number]->rear = rear;
        return;
    }else{
        temp = front;
        while(temp != NULL && temp != rear){
            
            if(temp->address == tag){
                temp->prev->next = temp->next;
                temp->next->prev = temp->prev;
                
                rear->next = temp;
                temp->prev = rear;
                rear = temp;
                rear->next = NULL;
                found = 1;
                set_data_global[cache_set_number]->rear = rear;
                break;
            }
            temp = temp->next;
        }
    }
    
    if(found == 0 && front->address){
        rear->next = (node_t *)calloc(1, sizeof(node_t));
        rear->next->prev = rear;
        rear = rear->next;
        rear->address = tag;
        rear->next = NULL;
        set_data_global[cache_set_number]->rear = rear;
    }
}

unsigned int get_least_used_tag_for_set(node_t *front_local, int cache_set_number){
    unsigned int return_address;
    
    return_address = 0;
    
    return_address = front_local -> address;
    front_local = front_local->next;
    front_local->prev = NULL;
    set_data_global[cache_set_number]->front = front_local;
    return return_address;
}


/*-----------------------HELPER METHODS FOR FULLY ASSOCIATIVE CACHE----------------------------*/

void update_usage(unsigned int tag){
    
    node_t *temp;
    int found;
    
    if(global_front == NULL){
        global_front = (node_t *)calloc(1, sizeof(node_t));
        global_front->prev = NULL;
        global_front->next = NULL;
        global_rear = global_front;
        global_front->address = tag;
        return;
    }
    
    if(global_rear->address == tag) return; /*already has the most recently used priority*/
    
    found = 0;
    
    if(global_front->address == tag && global_front != global_rear){
        temp = global_front;
        global_front = global_front->next;
        global_front->prev = NULL;
        
        global_rear->next = temp;
        temp->prev = global_rear;
        global_rear = temp;
        global_rear->next = NULL;
        return;
    }else{
        temp = global_front;
        while(temp != NULL && temp != global_rear){
            
            if(temp->address == tag){
                temp->prev->next = temp->next;
                temp->next->prev = temp->prev;
                
                global_rear->next = temp;
                temp->prev = global_rear;
                global_rear = temp;
                global_rear->next = NULL;
                found = 1;
                break;
            }
            temp = temp->next;
        }
    }
    
    if(found == 0 && global_front->address){
        global_rear->next = (node_t *)calloc(1, sizeof(node_t));
        global_rear->next->prev = global_rear;
        global_rear = global_rear->next;
        global_rear->address = tag;
        global_rear->next = NULL;
    }
}

unsigned int get_least_used_tag(){
    unsigned int return_tag;
    
    return_tag = 0;
    return_tag = global_front -> address;
    global_front = global_front->next;
    global_front->prev = NULL;
    return return_tag;
}



/*FOR TESTING*/

void printLL(node_t *front_local){
    printf("\n");
    while(front_local != NULL){
        /*printf("%lu |", front_local->address);*/
        front_local = front_local->next;
    }
    printf("\n");
}

int logarithm(unsigned int x){
    return log(x)/log(2);
}